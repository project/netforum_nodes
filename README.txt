README.txt 

netFORUM Dynamic Facade Nodes is what happens when an eMarketing Template
and a List content type get together and work together over xWeb.  It does a few neat things:

Lists are anything between {BeginList} and {EndList} tags.  All lists must have 
{BeginObjectName}, {EndObjectName}, {BeginDetail} and {EndDetail} tags, and 
if they are pulling a query from netFORUM's iWeb it should also have {BeginQueryName} 
and {EndQueryName} fields.  Otherwise it should have {BeginColumns}, {EndColumns}, 
{BeginWhere} and {EndWhere}.  If ALL of those tags are present, it will use the iWeb query
and ignore the rest.  These lists can be used to show the same kind of data that a child form
would, for example a Committee page might look like this:


{cmt_name} Members:
{BeginList}
{BeginObjectName}mb_committee_x_customer{EndObjectName}
{BeginColumns} cmc_cop_key, cst_name_cp {EndColumns}
{BeginWhere}cmc_cmt_key = '{cmt_key}' {EndWhere}
{BeginOrderBy}cst_sort_name{EndOrderBy}

{BeginDetail} <li> {cst_name_cp}, {cmc_cop_key}</li> 
{EndDetail}

{EndList}

But, that's not the end of the trick either.  Each object can also have a template, and if there
is a mb_committee_x_customer template, then this detail:

{BeginDetail} <li> {BeginLink}{cst_name_cp}{EndLink}, {cmc_cop_key}</li> 
{EndDetail}

Will link to the committee participant.  


SETUP
Since eWeb comes with many special curly brace tags, like {currentuserkey} and {objectkey}, 
at admin/settings/netforum/special-fields you can define any tag you want.  Many of the basic tags
are pre-populated and use PHP code to return values, such as {currentdate}