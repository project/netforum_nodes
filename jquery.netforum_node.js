var keepHelpCollapsed = false;

if (Drupal.jsEnabled){
  $(document).ready(function(){
    $('#edit-obj-type-name').bind("blur",setObjectType);
    $('#edit-key-submit').click(function(){return keyHelpSubmit();});
  });
}

  
function setObjectKey(obj_key){
  if (obj_key != ''){
    $("#edit-nfn-key").fadeOut("normal", function() { $(this).val(obj_key); }).fadeIn("normal");
  }
  return false;
}

function setObjectName(obj_type_name){
  obj_type_name += "";
  if (obj_type_name.toString().toLowerCase() != $('#edit-obj-type-name').val().toLowerCase() && obj_type_name != ''){
    $("#edit-obj-type-name").fadeOut("normal", function() { $(this).val(obj_type_name).blur(); }).fadeIn("normal");
  }
  return false;
}

function setObjectType(){
  //  If there's an autocomplete box up then wait till it closes to make our move.
  if ($('#autocomplete').length && $('#autocomplete').css("visibility") == 'visible'){
    return;
  }

  var objName = $(this).val();
  var prevObjName = '';
  if ( $('#edit-key-obj-type-name').length ) {
    prevObjName = $('#edit-key-obj-type-name').val();
  }
  if ( objName.toLowerCase() != prevObjName.toLowerCase()) {
   
      //update our autocomplete
      var fieldAutocomplete = location.protocol + "//" + location.host + BASE_URL + 'admin/content/netforum-object-fields-autocomplete/' + escape(objName);
      $('#edit-obj-field-help-autocomplete').val(fieldAutocomplete);
      //the url should have changed from something like http://www.example.com/admin/content/netforum-object-fields-autocomplete/9afef9a3-69fc-4c61-8114-02baefca77e7
      // to http://www.example.com/admin/content/netforum-object-fields-autocomplete/Individual
      
      //update our OTHER autocomplete
      var codeAutocomplete = location.protocol + "//" + location.host + BASE_URL + 'admin/content/netforum-template-code-autocomplete/' + escape(objName);
      $('#edit-template-code-autocomplete').val(codeAutocomplete );
      
      //Remove the previous autocompletes
      $('#edit-obj-field-help').unbind('keyup');
      $('#edit-obj-field-help').unbind('keydown');
      $('#edit-obj-field-help').unbind('blur');
      //yes, all of them
      $('#edit-template-code').unbind('keyup');
      $('#edit-template-code').unbind('keydown');
      $('#edit-template-code').unbind('blur');
      //re-bind the autocomplete actions to the form
      Drupal.autocompleteAutoAttach();
      
      if ($('#find_key_help').length){
        //Update the object search help if there's somewhere for it to go to
        keyHelpSubmit(); 
      }
    
  }  
  
  return false;
}

function keyHelpSubmit(newObjName) {
  //  If there's an autocomplete box up then wait till it closes to make our move
  if ($('#autocomplete').length && $('#autocomplete').css("visibility") == 'visible'){
    return;
  }
  
  if (! newObjName){
    newObjName = $('#edit-obj-type-name').val();
  }
  
  // Find any data they're submitting and package it into the get request
  var objLookupFields = [];
  if ( $('#edit-search-on').length ) {
    objLookupFields = Drupal.parseJson($('#edit-search-on').val());
  }
  var queryString = '';
  for ( item in objLookupFields){
    var objItem = objLookupFields[item].replace(/ /gi,""); //strip whitespace
    var formItem = '#edit-' + objItem.replace(/_/gi,"-").toLowerCase(); //replace underscores with dashes
    if ($(formItem).val()){
      queryString += objItem + "=" + escape($(formItem).val()) + "&";
    }
  }
  
  queryString += "url_lookup=" + escape($('#edit-url-lookup').val());
  

  uri = location.protocol + "//" + location.host + BASE_URL + 'admin/content/netforum-node-object-lookup/' + escape(newObjName);
  //I know that I didn't HAVE to build this query string, and that the null parameter to
  // $.get is the data, but I've already written it and it doesn't seem to be breaking
  //anything
  uri += "?" + queryString;
  
  //If the help fieldset is expanded, hide it during the query and show our
  //updating animation.
  var helpCollapsed = $('#object_help').is('.collapsed');
  if ( helpCollapsed != true && keepHelpCollapsed == false){ 
    Drupal.toggleFieldset($('#object_help')); 
    $('#object_help_status').show();
  }
  $('#find_key_help').html('');  //set the panel to be blank to keep them from firing two requests
  $.get(uri, null, function(data){ //get the data and do the following when it gets back
    $('#find_key_help').html(data); 
    //re-bind our events
    $('#edit-key-submit').click(function(){keepHelpCollapsed = false; return keyHelpSubmit();} ); 
    $('a.key-help').click(function(){return setObjectKey(this.id);} );
    $('a.obj-help').click(function(){  //this is the link they click on after searching based off of a netforum URL
          setObjectName($('#new_obj_name').html());
          setObjectKey($('#new_obj_key').html());
          keepHelpCollapsed = true;
          Drupal.toggleFieldset($('#object_help')); 
          return false;
    } );
    
    if (helpCollapsed != true && keepHelpCollapsed == false) { //display the help if it was showing before
      Drupal.toggleFieldset($('#object_help')); 
      $('#object_help_status').hide();
    }
  }); //end of the get 

  return false;
}
